
#!/usr/bin/env python
import time
import datetime
from influxdb import InfluxDBClient

def gettemp(id):
  try:
    mytemp = ''
    filename = 'w1_slave'
    f = open('/sys/bus/w1/devices/' + id + '/' + filename, 'r')
    line = f.readline() # read 1st line
    crc = line.rsplit(' ',1)
    crc = crc[1].replace('\n', '')
    if crc=='YES':
      line = f.readline() # read 2nd line
      mytemp = line.rsplit('t=',1)
    else:
      mytemp = 99999
    f.close()
 
    return int(mytemp[1])
 
  except:
    return 99999


#host = "192.168.178.54"
port = 8086
user = "root"
password = "root"
dbname = "logger"

# create influxdb client
client = InfluxDBClient('localhost', port, user, password, dbname)


# read sensor data
#for i in range(3):
while True:
  # Script has been called directly
  id = '28-01186cff10ff'
  temp_float = gettemp(id)/float(1000)
  print ('Temp : ' + '{:.3f}'.format(temp_float))

  temp = temp_float
  iso = time.ctime()

  # create json request
  json_body = [

    {

        "measurement": "test2",

        "tags": {

            "run": 1,

            "raspid": "raspi1"

        },

        "time": iso,

        "fields": {

            "temp": temp
        }

    }

  ]

  # Write JSON to InfluxDB
  client.switch_database(dbname)
  res = client.write_points(json_body)
  print(res)
  time.sleep(2)

print()
print (client.query('select temp from test;'))

