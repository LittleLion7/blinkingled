from influxdb import InfluxDBClient

client = InfluxDBClient(host='localhost', port=8086)
#client.create_database('pyexample')
#client.get_list_database()
result=client.query('SELECT * FROM test')
print(result)
client.query('INSERT temperature,location=living_room3 value=21')


'''
json_body = [

    {

        "measurement": "brushEvents",

        "tags": {

            "user": "Carol",

            "brushId": "6c89f539-71c6-490d-a28d-6c5d84c0ee2f"

        },

        "time": "2018-03-28T8:01:00Z",

        "fields": {

            "duration": 127

        }

    },

    {

        "measurement": "brushEvents",

        "tags": {

            "user": "Carol",

            "brushId": "6c89f539-71c6-490d-a28d-6c5d84c0ee2f"

        },

        "time": "2018-03-29T8:04:00Z",

        "fields": {

            "duration": 132

        }

    },

    {

        "measurement": "brushEvents",

        "tags": {

            "user": "Carol",

            "brushId": "6c89f539-71c6-490d-a28d-6c5d84c0ee2f"

        },

        "time": "2018-03-30T8:02:00Z",

        "fields": {

            "duration": 129

        }

    }

]

client.switch_database('pyexample')
client.write_points(json_body)

result = client.query('SELECT "duration" FROM "pyexample"."autogen"."brushEvents" WHERE time > now() - 4d GROUP BY "user"')
print(result.raw)
'''
